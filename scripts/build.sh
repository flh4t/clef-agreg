#!/bin/bash

# cloud-localds (from cloud-image-utils in Debian) creates a disk image (seed.img) for cloud-init
cloud-localds seed.img cloud-config.yaml
# Download Ubuntu 20.04 LTS (focal)
wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
qemu-img resize focal-server-cloudimg-amd64.img +14G
# Boot the Ubuntu cloud image with qemu passing the cloud-init disk image to configure it 
qemu-system-x86_64 -m 8192 -smp 4 -net nic -net user -hda focal-server-cloudimg-amd64.img -hdb seed.img -nographic
# convert the qemu format to VirtualBox format
qemu-img convert focal-server-cloudimg-amd64.img focal-server-cloudimg-amd64.raw
VBoxManage convertdd focal-server-cloudimg-amd64.raw focal-server-cloudimg-amd64.vdi --format VDI
